import asyncio
import aiohttp
import time

async def send_post_request(session, url, data):
    async with session.post(url, json=data) as response:
        print(f"Status: {response.status}, URL: {url}")
        return await response.text()

async def main():
    url = "https://sc20dlcwk2.azurewebsites.net/api/function1"
    #url = "http://20.68.242.142:8080/function/function1"
    data = {"sentence":"I am very happy","email":"sc20dlnoreply@gmail.com"}
    tasks = []

    async with aiohttp.ClientSession() as session:
        for _ in range(500):
            task = asyncio.ensure_future(send_post_request(session, url, data))
            tasks.append(task)

        await asyncio.gather(*tasks)

if __name__ == "__main__":
    start_time = time.perf_counter()
    asyncio.run(main())
    elapsed_time = time.perf_counter() - start_time
    print(f"The task took {elapsed_time} seconds.")
