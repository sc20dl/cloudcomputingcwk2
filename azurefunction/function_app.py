import azure.functions as func
import logging, requests, json, pymssql, smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

app = func.FunctionApp(http_auth_level=func.AuthLevel.ANONYMOUS)

@app.route(route="function1")
def function1(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function1 processed a request.')
    req_body = req.get_json()
    sentence, email = req_body["sentence"], req_body["email"]

    url = "https://twinword-sentiment-analysis.p.rapidapi.com/analyze/"
    payload = { "text": sentence }

    headers = {
        "content-type": "application/x-www-form-urlencoded",
        "X-RapidAPI-Key": "78c1ef2570mshf81ceb4688ec3c4p1cfd11jsna29093b34962",
        "X-RapidAPI-Host": "twinword-sentiment-analysis.p.rapidapi.com"
    }
    response = requests.post(url, data=payload, headers=headers).json()
    
    data = {}
    if response["result_msg"] == "Success":
        data["msg"] = "Request was successfull"
        data["score"] = response["score"]
        data["type"] = response["type"]
        data["keywords"] = response["keywords"]

        d = {"sentence":sentence,
            "score":response["score"],
            "sentiment":response["type"],
            "email":email}
        
        headers = {'Content-Type': 'application/json'}
        try:
            result = requests.post("https://sc20dlcwk2.azurewebsites.net/api/function2", headers=headers, json=d).json()
            return func.HttpResponse(
        body=json.dumps({'data':data,"msg":result}),
        status_code=200,
        headers={"Content-Type": "application/json"}
    )
        except Exception as e:
            return func.HttpResponse(
            body=json.dumps({"msg":e}),
            status_code=400,
            headers={"Content-Type": "application/json"}
        )
    else:
        return func.HttpResponse(
            body=json.dumps({'success':False,"msg":"Unable to connect to API"}),
            status_code=400,
            headers={"Content-Type": "application/json"}
        )

@app.route(route="function2", auth_level=func.AuthLevel.ANONYMOUS)
def function2(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')
    json_req = req.get_json()
    sentence, email, score, sentiment = json_req["sentence"], json_req["email"], json_req["score"], json_req["sentiment"]

    server = 'sc20dlcwk2.database.windows.net'
    database = 'cwk'
    username = 'sc20dl@sc20dlcwk2'
    password = 'Divockorigi!'

    try:
        conn = pymssql.connect(server, username, password, database)
        cursor = conn.cursor()
                
        query = "INSERT INTO sentiment (sentence, score) VALUES (%s, %s)"
        cursor.execute(query, (sentence, score))

        conn.commit()
        cursor.close()
        conn.close()
        d = {"sentence":sentence,
            "score":score,
            "sentiment":sentiment,
            "email":email}
        
        headers = {'Content-Type': 'application/json'}
        try:
            result = requests.post("https://sc20dlcwk2.azurewebsites.net/api/function3", headers=headers, json=d).json()
            return func.HttpResponse(
            body=json.dumps(result),
            status_code=200,
            headers={"Content-Type": "application/json"}
        )
        except Exception as e:
            return func.HttpResponse(
            body=json.dumps({"msg":e}),
            status_code=400,
            headers={"Content-Type": "application/json"}
        )
    except pymssql.DatabaseError as e:
        return func.HttpResponse(
            body=json.dumps({'success':False, "msg":"Unable to connect/write to DB"}),
            status_code=400,
            headers={"Content-Type": "application/json"}
        )


@app.route(route="function3", auth_level=func.AuthLevel.ANONYMOUS)
def function3(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')
    json_req = req.get_json()
    sentence, email, score, sentiment = json_req["sentence"], json_req["email"], json_req["score"], json_req["sentiment"]

    sender_email = "sc20dlnoreply@gmail.com"
    receiver_email = email
    password = "crbz sics isnc tvay"

    message = MIMEMultipart("alternative")
    message["Subject"] = "Scores"
    message["From"] = sender_email
    message["To"] = receiver_email

    text = """\
    Hi, the sentence "{}" was classified as {} and had a score of {}.""".format(sentence, sentiment, score)
    
    html = """\
    <html>
    <body>
        <p>Hi,<br>
        The sentence "{}" was classified as {} and had a score of {}.
        </p>
    </body>
    </html>
    """.format(sentence, sentiment, score)

    part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")

    message.attach(part1)
    message.attach(part2)

    try:
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, password)
            server.sendmail(
                sender_email, receiver_email, message.as_string()
            )
        return func.HttpResponse(
            body=json.dumps({'success':True,"msg":"Email successfully sent!"}),
            status_code=200,
            headers={"Content-Type": "application/json"}
        )
    except Exception as e:
        return func.HttpResponse(
            body=json.dumps({'success':False,"msg":"Unable to send email"}),
            status_code=400,
            headers={"Content-Type": "application/json"}
        )
