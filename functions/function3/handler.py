import json, smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def handle(event, context):
    json_req = json.loads(event.body)
    sentence, email, score, sentiment = json_req["sentence"], json_req["email"], json_req["score"], json_req["sentiment"]

    sender_email = "sc20dlnoreply@gmail.com"
    receiver_email = email
    password = "crbz sics isnc tvay"

    message = MIMEMultipart("alternative")
    message["Subject"] = "Scores"
    message["From"] = sender_email
    message["To"] = receiver_email

    text = """\
    Hi, the sentence "{}" was classified as {} and had a score of {}.""".format(sentence, sentiment, score)
    
    html = """\
    <html>
    <body>
        <p>Hi,<br>
        The sentence "{}" was classified as {} and had a score of {}.
        </p>
    </body>
    </html>
    """.format(sentence, sentiment, score)

    part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")

    message.attach(part1)
    message.attach(part2)

    try:
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, password)
            server.sendmail(
                sender_email, receiver_email, message.as_string()
            )
        return json.dumps({'success':True,"msg":"Email successfully sent!"})
    except Exception as e:
        return json.dumps({'success':False,"msg":"Unable to send email"})
