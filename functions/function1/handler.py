import requests, json

def handle(event, context):
    json_req = json.loads(event.body)
    sentence, email = json_req["sentence"], json_req["email"]
    print(sentence, email)
    url = "https://twinword-sentiment-analysis.p.rapidapi.com/analyze/"
    payload = { "text": sentence }

    headers = {
        "content-type": "application/x-www-form-urlencoded",
        "X-RapidAPI-Key": "78c1ef2570mshf81ceb4688ec3c4p1cfd11jsna29093b34962",
        "X-RapidAPI-Host": "twinword-sentiment-analysis.p.rapidapi.com"
    }
    response = requests.post(url, data=payload, headers=headers).json()
    print(response)

    data = {}
    if response["result_msg"] == "Success":
        data["msg"] = "Request was successfull"
        data["score"] = response["score"]
        data["type"] = response["type"]
        data["keywords"] = response["keywords"]

        # send req to 2nd function (write to DB)
        d = {"sentence":sentence,
            "score":response["score"],
            "sentiment":response["type"],
            "email":email}
        
        headers = {'Content-Type': 'application/json'}
        try:
            result = requests.post("http://20.68.242.142:8080/function/function2", headers=headers, json=d).json()
            return json.dumps({'data':data,"msg":result})
        except Exception as e:
            return json.dumps({"msg":e})
    else:
        return json.dumps({'success':False,"msg":"Unable to connect to API"})