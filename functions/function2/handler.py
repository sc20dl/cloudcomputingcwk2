import pymssql, json, requests

def handle(event, context):
    json_req = json.loads(event.body)
    sentence, email, score, sentiment = json_req["sentence"], json_req["email"], json_req["score"], json_req["sentiment"]
    
    server = 'sc20dlcwk2.database.windows.net'
    database = 'cwk'
    username = 'sc20dl@sc20dlcwk2'
    password = 'Divockorigi!'  

    try:
        conn = pymssql.connect(server, username, password, database)
        cursor = conn.cursor()
                
        query = "INSERT INTO sentiment (sentence, score) VALUES (%s, %s)"
        cursor.execute(query, (sentence, score))

        conn.commit()
        cursor.close()
        conn.close()
        d = {"sentence":sentence,
            "score":score,
            "sentiment":sentiment,
            "email":email}
        
        headers = {'Content-Type': 'application/json'}
        try:
            result = requests.post("http://20.68.242.142:8080/function/function3", headers=headers, json=d).json()
            return json.dumps(result)
        except Exception as e:
            return json.dumps({"msg":e})
    except pymssql.DatabaseError as e:
        return json.dumps({'success':False, "msg":"Unable to connect/write to DB"})
